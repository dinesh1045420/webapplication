package com.frontend.Dinesh.models;


import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collection;

@Data
@Document(collection  = "roles")
@EqualsAndHashCode(callSuper = true)
public class MarkdownRoleModel extends  GenericModel{

    private  String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public MarkdownRoleModel() {
        super();
    }
}
