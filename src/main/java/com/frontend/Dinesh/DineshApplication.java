package com.frontend.Dinesh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@EnableMongoAuditing
@SpringBootApplication
public class  DineshApplication {

	public static void main(String[] args) {
		SpringApplication.run(DineshApplication.class, args);
	}

}
